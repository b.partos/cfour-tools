'''
Created on Jul 3, 2018

@author: balazs
'''

class GetterSetterGenerator(object):
  
    def generateConstructor(self, attributeList):
      print("def __init__(self):")
      for attribute in attributeList:
        print("  self.__"+attribute+" = None")
      print()

    def generateStandardGetters(self, attributeList):
      for attribute in attributeList:
        print("@property")
        print("def "+attribute+"(self):")
        print("  return  self.__"+attribute)
        print()
      print()
      for attribute in attributeList:
        print("@"+attribute+".setter")
        print("def "+attribute+"(self, "+attribute+"):")
        print("  self.__"+attribute+" = "+attribute)
        print()

