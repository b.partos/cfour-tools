'''
Created on Jul 2, 2018

@author: balazs
'''

from Cfour.model.CartesianGaussTypeBasisFunction import CartesianGaussTypeBasisFunction as CGTBF
from Cfour.model.Coordinates import Coordinates
from Cfour.model.PrimitiveGaussTypeBasisFunction import PrimitiveGaussTypeBasisFunction as PGTBF

testCGTBF = CGTBF(Coordinates(0,0,0),0,0,0)

testCGTBF.addPrimitiveGaussTypeBasisFunction(PGTBF(0.256, 1.2))

# testCGTBF2 = CGTBF(2,0,0,0)

def func1(par):
  print("Func1",par)
  
def func2(par):
  print("Func2",par)
  
funcs = {"func1":func1,"func2":func2}

print(funcs["func1"](25))
print("wut?")