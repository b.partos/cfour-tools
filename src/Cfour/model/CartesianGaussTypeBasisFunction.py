'''
Created on Apr 24, 2018

@author: balazs
'''

import array

from Cfour.model.Coordinates import Coordinates
from Cfour.model.PrimitiveGaussTypeBasisFunction import PrimitiveGaussTypeBasisFunction as PGTO

class CartesianGaussTypeBasisFunction(object):
  '''
  classdocs
  '''


  def __init__(self, coordinates, xP, yP, zP):
    '''
    Constructor
    '''
    if(type(coordinates)==Coordinates):
      self.__paramters = array.array('d',[coordinates.x,coordinates.y,coordinates.z,xP,yP,zP])
      self.__primitiveGaussTypeBasisFunctions = []
    else:
      raise ValueError("coordinates has to be of type Coordinates!")
  
  def addPrimitiveGaussTypeBasisFunction(self, primitiveGaussTypeBasisFunction):
    if(type(primitiveGaussTypeBasisFunction)==PGTO):
      self.__primitiveGaussTypeBasisFunctions.append(primitiveGaussTypeBasisFunction)
    else:
      raise ValueError
  
  
  
  @property
  def coordiantes(self):
    return Coordinates(self.paramters[0], self.paramters[1], self.paramters[2])
  
  @property
  def xExponent(self):
    return self.__paramters[3]
  
  @property
  def yExponent(self):
    return self.__paramters[4]
      
  @property
  def zExponent(self):
    return self.__paramters[5]


'''
class Point:
  def __init__(self, x, y, z):
    self.x=np.float64(x)
    self.y=np.float64(y)
    self.z=np.float64(z)
  

  def distance(self, point2):
    return np.sqrt((point2.x-self.x)**2+(point2.y-self.y)**2+(point2.z-self.z)**2)

class PrimitiveGaussian(Point):
  
  def __init__(self, coeff, zeta, x, y, z ):
    Point.__init__(self, x, y, z)
    self.zeta = np.float64(zeta)
    self.coeff = np.float64(coeff)

  def getValue(self, x, y, z):
    r = np.sqrt((x-self.x)**2+(y-self.y)**2+(z-self.z)**2)
    return coeff*np.exp(-r**2)
  
  def gaussianProduct(self, gaussian2):
    proportionalityConstant = np.exp(-self.zeta*gaussian2.zeta/
                                     (self.zeta+gaussian2.zeta)*
                                     (self.distance(gaussian2)))
    
  
  
'''