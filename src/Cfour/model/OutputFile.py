'''
Created on May 1, 2018

@author: balazs
'''
from Cfour.model.OutputFileIndex import OutputFileIndex

class OutputFile(object):
  '''
  Class to wrap the raw data contained in the CFOUR files
  '''


  def __init__(self, filePath):
    '''
    Constructor
    '''
    self.string = ""
    self.wordList = []
    
    with open(filePath) as outputFileObject:
      self.__string = outputFileObject.read()
      for line in self.string.splitlines():
        self.__wordList.append(line.split())
    self.__stringSize = len(self.string)
    self.__wordListSize = len(self.wordList)
    self.__outputFileIndex = OutputFileIndex(self)
    
  @property
  def wordList(self):
    return self.wordList
    
  @property
  def stringSize(self):
    return self.stringSize
  
  @property
  def wordListSize(self):
    return self.wordListSize
  
  @property
  def string(self):
    return self.string
  
  def getWordByCoordinates(self, lineNumber, wordIndex):
    return self.wordList[lineNumber][wordIndex]
  
  def getStringByStartAndEnd(self, startIndex, endIndex):
    return self.string[startIndex, endIndex]
     
    
    
  